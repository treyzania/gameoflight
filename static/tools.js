var toolDraw = {
    onSwitchTo: function() {
	console.log("switched to draw tool");
    },
    onSwitchFrom: function(next) {
    },
    onMouseMove: function(e, tx, ty) {
    },
    onClick: function(e, tx, ty) {

	if (keys["shift"]) {
	    removeCellFromPending(tx, ty);
	} else {
	    for (let i = 0; i < userCurrentTemplate.length; i++) {
		tryAddCellToPending(tx + userCurrentTemplate[i].x, ty + userCurrentTemplate[i].y);
	    }
	}

    }
}

var defaultTool = "draw";

var TOOLS = {
    "nulltool": null,
    "draw": toolDraw,
}

function tryAddCellToPending(x, y) {

    console.log("wanting to add " + x + " " + y + " to pending cells");

    if (x < 0 || x >= WORLD_WIDTH || y < 0 || y >= WORLD_HEIGHT) {
	console.log("out of bounds");
	return;
    }

    // First check to see if we already have it.
    for (let i = 0; i < userDraw.length; i++) {
	if (userDraw[i].x == x && userDraw[i].y == y) {
	    return;
	}
    }

    console.log("adding...");

    // If not, then actually add it.
    userDraw.push({
	x: x,
	y: y
    });

    // And update the UI.
    updateSatoshiCostDisplay();
}

function removeCellFromPending(x, y) {

    for (let i = 0; i < userDraw.length; i++) {
	if (userDraw[i].x == x && userDraw[i].y == y) {
	    userDraw.splice(i, 1);
	    updateSatoshiCostDisplay();
	    return true;
	}
    }

    return false;

}
