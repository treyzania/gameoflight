
// This isn't used yet.  Will do when we implement culling properly.
const RENDER_CULL_BUFFER = 3;

const COLORS = [
    "#000000",
    "#0000ff",
    "#00ff00",
    "#00ffff",
    "#ff0000",
    "#ff00ff",
    "#ffff00",
    "#666666"
];

const COLORS_PENDING = [
    "#777777",
    "#aaaaff",
    "#aaffaa",
    "#aaffff",
    "#ffaaaa",
    "#ffaaff",
    "#ffffaa",
    "#cccccc"
]

const COLORS_TERRITORY = [
    "#ffffff", // ehhh?
    "#ddddff",
    "#ddffdd",
    "#ddffff",
    "#ffdddd",
    "#ffddff",
    "#ffffdd",
    "#e7e7e7"
]

var cellCanvas = null;
var cellsNeedsRedraw = true;

var worldCanvas = null;
var screenCanvas = null;

var renderTicks = true;
var tickIncr = 5;
var tickZoomLevel = 24;

var borderBuffer = 128;
var minZoom = 4;
var maxZoom = 72;

function renderCellInContext(cells, ctx, x, y) {

    if (x < 0 || x >= WORLD_WIDTH || y < 0 || y >= WORLD_HEIGHT) {
	return false;
    }

    let cell = cells[x][y];

    if (cell) { // "truthyness" for real things

	if (!debug.debugCellColors) {
	    // Depending on if the cell is live we use one palette or another.
	    if (cell.live) {
		ctx.fillStyle = COLORS[cell.color];
	    } else {
		ctx.fillStyle = COLORS_TERRITORY[cell.color];
	    }
	} else {
	    // Debug colors.
	    if (cell.live) {
		ctx.fillStyle = "#303030";
	    } else {
		ctx.fillStyle = "#e0e0e0";
	    }
	}

	// Then actually render after we've eventually decided what to render.
	ctx.fillRect(x, y, 1, 1); // just 1 pixel

	return true;

    } else {
	return false;
    }

}

function renderCellsToContext(cells, ctx, minX, minY, maxX, maxY, offX, offY) {

    let rendered = 0;

    for (let x = minX; x <= maxX; x++) {

	if (x < 0 || x >= cells.length) {
	    continue;
	}

	let col = cells[x];

	for (let y = minY; y <= maxY; y++) {

	    if (y < 0 || y >= col.length) {
		continue;
	    }

	    let didRender = renderCellInContext(cells, ctx, x, y);
	    if (didRender) {
		rendered++;
	    }

	}
    }

    if (debug.printTilesRendered) {
	console.log("drew " + rendered + " cells");
    }

    // return the number of actual cells rendered.
    return rendered;

}

/// Updates a single cell.
function updateCellsCanvasAt(x, y) {
    let cctx = cellsCanvas.getContext("2d");
    cctx.imageSmoothingEnabled = false;
    return renderCellInContext(gWorldState, cctx, x, y);
}

/// Takes a list of coordinates to update.
function updateCellsCanvasAt_batched(points) {
    let cctx = cellsCanvas.getContext("2d");
    cctx.imageSmoothingEnabled = false;

    for (let i = 0; i < points.length; i++) {
	renderCellInContext(gWorldState, cctx, points[i].x, points[i].y);
    }
}

/// This function takes two viewports and renders cells in the first one but not
/// the second one.
function updateCellsCanvasInExpandingViewport(cam, prevCam) {

    let bounds = getCameraWorldBounds(cam);
    let prevBounds = getCameraWorldBounds(prevCam);
    let rendered = 0;

    let cctx = cellsCanvas.getContext("2d");
    cctx.imageSmoothingEnabled = false;

    // Check to see if we should expand this by 1 more cell.
    for (let x = Math.max(bounds.minx, 0); x <= Math.min(bounds.maxx, WORLD_WIDTH); x++) {
	for (let y = Math.max(bounds.miny, 0); y <= Math.min(bounds.maxy, WORLD_HEIGHT); y++) {

	    // Isn't super efficent, we should entirely skip the inner camera.
	    if (doesBoundContainClosed(prevBounds, x, y)) {
		continue;
	    }

	    let didRender = renderCellInContext(gWorldState, cctx, x, y);
	    if (didRender) {
		rendered++;
	    }

	}
    }

    return rendered;
}

/// Updates all the cells that the camera can see.
function updateCellsCanvasInViewport(cam) {

    let bounds = getCameraWorldBounds(cam);
    let rendered = 0;

    let cctx = cellsCanvas.getContext("2d");
    cctx.imageSmoothingEnabled = false;

    for (let x = Math.max(bounds.minx, 0); x < Math.min(bounds.maxx, WORLD_WIDTH); x++) {
	for (let y = Math.max(bounds.miny, 0); y < Math.min(bounds.maxy, WORLD_WIDTH); y++) {

	    let didRender = renderCellInContext(gWorldState, cctx, x, y);
	    if (didRender) {
		rendered++;
	    } else {
		console.log("didn't render a cell we thought we would at " + x + " " + y);
	    }

	}
    }

    return rendered;
}

/// Takes a viewport and a list of points and redraws only the points that are
/// in the viewport.
function updateCellsCanvasInViewport_selective(cam, points) {

    let bounds = getCameraWorldBounds(cam);
    let rendered = 0;

    let cctx = cellsCanvas.getContext("2d");
    cctx.imageSmoothingEnabled = false;

    for (let i = 0; i < points.length; i++) {
	let p = points[i];
	if (isInOpenRange(bounds.minx - 1, bounds.maxx + 1, p.x) && isInOpenRange(bounds.miny - 1, bounds.maxy + 1, p.y)) {
	    let didRender = renderCellInContext(gWorldState, cctx, p.x, p.y)
	    if (didRender) {
		rendered++;
	    }
	}
    }

    return rendered;
}

/// Updates the cells canvas in the specified region from our "cacnonical"
/// representation of the world.
function updateCellsCanvasRegion(xmin, ymin, xmax, ymax) {
    // Get the screen context.
    let cctx = cellsCanvas.getContext("2d");
    cctx.imageSmoothingEnabled = false;

    // Actually apply them.
    return renderCellsToContext(gWorldState, cctx, xmin, ymin, xmax, ymax, 0, 0);
}

function updateCellsCanvasAll() {
    return updateCellsCanvasRegion(0, 0, WORLD_WIDTH - 1, WORLD_HEIGHT - 1);
}

function renderWorldToContext(ctx) {

    ctx.imageSmoothingEnabled = false;
    let camBounds = getCameraWorldBounds(cameraState);

    // First, draw the mouse position, in case we need it.
    let worldMouse = convertScreenSpaceToWorldSpace(mouse);
    worldMouse.x = Math.floor(worldMouse.x);
    worldMouse.y = Math.floor(worldMouse.y);

    // Now render the cell canvas to the world canvas.
    ctx.drawImage(cellsCanvas, 0, 0);

    // Render ruler marks every 5 cells.
    // TODO Make this only render tick marks in view, as this does waaaay more
    // work than it needs to since the tick marks are usually very few.
    if (renderTicks && cameraState.zoom > tickZoomLevel) {
	ctx.fillStyle = "#f0f0f0";
	let sx = Math.floor(camBounds.minx / tickIncr) * tickIncr;
	let fx = Math.ceil(camBounds.maxx / tickIncr) * tickIncr;
	let sy = Math.floor(camBounds.miny / tickIncr) * tickIncr;
	let fy = Math.ceil(camBounds.maxy / tickIncr) * tickIncr;
	for (let x = sx; x < fx; x += tickIncr) {
	    for (let y = sy; y < fy; y += tickIncr) {
		if (!gWorldState[x][y].live) {
		    ctx.fillRect(x, y, 1, 1);
		}
	    }
	}
    }

    // Now draw the cell(s) at the cursor.
    ctx.fillStyle = COLORS_PENDING[userColor];
    if (keys["shift"]) {
	ctx.fillRect(worldMouse.x, worldMouse.y, 1, 1);
    } else {
	for (let i = 0; i < userCurrentTemplate.length; i++) {
	    let tc = userCurrentTemplate[i];
	    ctx.fillRect(worldMouse.x + tc.x, worldMouse.y + tc.y, 1, 1);
	}
    }

    // Draw scratch cells, if there are any.
    if (userDraw.length > 0) {
	for (let i = 0; i < userDraw.length; i++) {
	    let cell = userDraw[i];
	    ctx.fillRect(cell.x, cell.y, 1, 1);
	}
    }

    // Draw cells pending payment, pretty light color.
    // There's a simpler way to do this but for some reason it doesn't like me.
    /*ctx.fillStyle = "#e0e0e0";
    let pkeys = Object.keys(pendingDraws);
    for (let i = 0; i < pkeys.length; i++) {
	let pd = pendingDraws[pkeys[i]];
	for (let j = 0; j < pd.length; j++) {
	    ctx.fillRect(pd[j].x, pd[j].y, 1, 1);
	}
    }*/

    // Draw the edit window, if there is one.
    /*if (gEditWindow != null) {
	let ew = gEditWindow;

	// FIXME This ends up making the border blurry because of reasons.
	ctx.fillStyle = "rgba(0, 187, 0, 0.2)";
	ctx.fillRect(ew.xpos, ew.ypos, ew.width, ew.height);
	ctx.strokeStyle = "#222222";
	ctx.strokeRect(ew.xpos, ew.ypos, ew.width, ew.height);
    }*/

}

var lastWorldWidth = -1;
var lastWorldHeight = -1;

/// This updates the canvas for the world space, where most stuff actually
/// happens.  This is transformed based on where the camera is later.
function updateWorldSpaceCanvas() {

    // In case something changed with the world size, update it.
    if (WORLD_WIDTH != lastWorldWidth || WORLD_HEIGHT != lastWorldHeight) {
	worldCanvas.width = WORLD_WIDTH;
	worldCanvas.height = WORLD_HEIGHT;
	lastWorldWidth = WORLD_WIDTH;
	lastWorldHeight = WORLD_HEIGHT;
    }

    // Setup render context.
    let wctx = worldCanvas.getContext("2d");
    wctx.imageSmoothingEnabled = false;

    // Clear it.
    wctx.fillStyle = "#ffffff";
    wctx.fillRect(0, 0, worldCanvas.width, worldCanvas.height);

    // Now redraw everything.
    if (!debug.noRenderGameState) {
	renderWorldToContext(wctx);
    }

    // Draw a border so we know where the edges are.
    wctx.fillStyle = "#000000";
    wctx.beginPath()
    wctx.moveTo(0, 0);
    wctx.lineTo(0, worldCanvas.height);
    wctx.lineTo(worldCanvas.width, worldCanvas.height);
    wctx.lineTo(worldCanvas.width, 0);
    wctx.lineTo(0, 0);
    wctx.stroke();

}

/// This updates the canvas used as "screen space".  This canvas gets blitted to
/// the screen canvas directly without any transformations and is just used to
/// avoid drawing to an active object.
function updateScreenSpaceCanvas() {

    // Setup render context.
    let sctx = screenCanvas.getContext("2d");
    sctx.imageSmoothingEnabled = false;
    let screenWidth = screenCanvas.width;
    let screenHeight = screenCanvas.height;

    // Clear it.
    sctx.fillStyle = "#ffffff";
    sctx.fillRect(0, 0, screenWidth, screenHeight);
    sctx.fillStyle = "#000000";

    /*
     * Now figure out where to put the camera.  This is basically just a bunch
     * of geometry, it's pretty bad but whatever.  Remember, the "zoom" is
     * defined as how many pixels the edge of 1 tile should take up.  We can
     * figure out everything else from there.
     */

    let zoom = cameraState.zoom;
    let cellsHoriz = screenWidth / zoom;
    let cellsVert = screenHeight / zoom;

    let worldX = cameraState.x - (cellsHoriz / 2); // XXX
    let worldY = cameraState.y - (cellsVert / 2); // XXX

    // Actually render it.  (Some transform stack muckery going on here.)
    sctx.save();
    sctx.scale(zoom, zoom);
    sctx.drawImage(worldCanvas, worldX * -1, worldY * -1);
    sctx.restore();

    // A "minimap".
    if (debug.drawMinimap) {
	let minimapX = screenWidth - 360;
	let minimapY = screenHeight - WORLD_HEIGHT - 36;

	// Draw the actual minimap
	sctx.drawImage(worldCanvas, minimapX, minimapY);

	// Draw a box representing the camera on top.
	let camBounds = getCameraWorldBounds(cameraState)
	sctx.strokeStyle = "#0000FF";
	sctx.strokeRect(
	    camBounds.minx + minimapX,
	    camBounds.miny + minimapY,
	    cellsHoriz,
	    cellsVert);
    }

}

var lastCanvasWidth = -1;
var lastCanvasHeight = -1;

/// This actually updates the screen canvas from the DOM.
function updateDisplay() {

    let out = document.getElementById("game");

    // Cache these since it avoids potentially updating the DOM layout if we don't have to.
    let dw = document.body.clientWidth;
    let dh = document.body.clientHeight;
    if (dw != lastCanvasWidth || dh != lastCanvasHeight) {
	out.width = dw;
	out.height = dh;
	screenCanvas.width = dw;
	screenCanvas.height = dh;
	lastCanvasWidth = dw;
	lastCanvasHeight = dh;
    }

    let ctx = out.getContext("2d");
    ctx.imageSmoothingEnabled = false;

    // First clear it.
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, out.width, out.height);

    // Now draw it.
    ctx.drawImage(screenCanvas, 0, 0);

}

function lerp(a, b, t) {
    let ct = 0;
    if (t > 1) {
	ct = 1;
    } else if (t < 0) {
	ct = 0;
    } else {
	ct = t;
    }
    return a * (1.0 - ct) + b * ct;
}

var frames = 0;
var lastFrameTime = -1;
var fps = 1.0;

function runFrameUpdate(time) {

    // Do some time math.
    let dt = time - lastFrameTime
    fps = 1000.0 / dt;
    lastFrameTime = time;

    // Don't do any camera manipulation until we've got a stable FPS value.
    if (frames > 2) {

	// Apply movements.
	if (keys["d"]) {
	    cameraTarget.x += (1.0 / dt) * CAM_MOVE_SPEED / cameraState.zoom;
	}

	if (keys["w"]) {
	    cameraTarget.y -= (1.0 / dt) * CAM_MOVE_SPEED / cameraState.zoom;
	}

	if (keys["a"]) {
	    cameraTarget.x -= (1.0 / dt) * CAM_MOVE_SPEED / cameraState.zoom;
	}

	if (keys["s"]) {
	    cameraTarget.y += (1.0 / dt) * CAM_MOVE_SPEED / cameraState.zoom;
	}

	if (keys["e"]) {
	    cameraTarget.zoom *= CAM_ZOOM_MULT;
	}

	if (keys["r"]) {
	    cameraTarget.zoom /= CAM_ZOOM_MULT;
	}

	// Now do clamping so we don't get lost.
	if (cameraTarget.x < -borderBuffer) {
	    cameraTarget.x = -borderBuffer;
	}

	if (cameraTarget.x > WORLD_WIDTH + borderBuffer) {
	    cameraTarget.x = WORLD_WIDTH + borderBuffer;
	}

	if (cameraTarget.y < -borderBuffer) {
	    cameraTarget.y = -borderBuffer;
	}

	if (cameraTarget.y > WORLD_HEIGHT + borderBuffer) {
	    cameraTarget.y = WORLD_HEIGHT + borderBuffer;
	}

	if (cameraTarget.zoom < minZoom) {
	    cameraTarget.zoom = minZoom;
	}

	if (cameraTarget.zoom > maxZoom) {
	    cameraTarget.zoom = maxZoom;
	}

	// Calculate the new position of the camera.
	let nextCam = {};
	for (var p in cameraTarget) {
	    nextCam[p] = lerp(cameraState[p], cameraTarget[p], (1.0 / fps) * ZOOM_SPRINGYNESS);
	}

	// Rerender expanding things if necessary.
	if (!isBoundsEqual(getCameraWorldBounds(cameraState), getCameraWorldBounds(nextCam))) {
	    let start = window.performance.now();
	    let redraws = updateCellsCanvasInExpandingViewport(nextCam, cameraState);
	    let fin = window.performance.now();
	    if (redraws > 0 && debug.printTilesRendered) {
		console.log("[cam move] redrew " + redraws + " cells (took " + (fin - start) + " millis)");
	    }
	}

	// Replace the camera state with the new one.
	cameraState = nextCam;

    }

    // Then actually update the canvas.
    // (this way of doing it is absolutely awful, I'm so sorry)
    updateWorldSpaceCanvas();
    updateScreenSpaceCanvas();
    updateDisplay();
    frames++;

    // Now set ourselves to be called again on the next frame.
    window.requestAnimationFrame(runFrameUpdate)

}

function setViewport(x, y, z) {
    cameraTarget = {
	x: x,
	y: y,
	zoom: z
    };
    cameraState = {
	x: x,
	y: y,
	zoom: z
    };
}

function getCameraWorldBounds(cam) {
    return {
	minx: Math.floor(cam.x - lastCanvasWidth / cam.zoom / 2),
	miny: Math.floor(cam.y - lastCanvasHeight / cam.zoom / 2),
	maxx: Math.ceil(cam.x + lastCanvasWidth / cam.zoom / 2),
	maxy: Math.ceil(cam.y + lastCanvasHeight / cam.zoom / 2)
    };
}

function isInOpenRange(min, max, v) {
    return min < v && v < max;
}

function isBoundsEqual(ab, bb) {
    return ab.minx == bb.minx && ab.maxx == bb.maxx && ab.miny == bb.miny && ab.maxy == bb.maxy;
}

function doesBoundContainClosed(bound, x, y) {
    return bound.minx <= x && x <= bound.maxx && bound.miny <= y && y <= bound.maxy;
}

function getBoundsArea(bounds) {
    return (bounds.maxx - bounds.minx) * (bounds.maxy - bounds.miny);
}
