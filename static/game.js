/*
 * Hey, you're looking at my code!
 *
 * In case you don't know me, I absolutely dispise JavaScript.  All the backend
 * for this website is written in Rust since it's the coolest thing since sliced
 * bread.
 *
 * Anyways, go ahead and keep digging through this code and have fun!  Don't do
 * anything evil with it please.  https://gitlab.com/treyzania/gameoflight
 */

/*
 * Game of Light, it's satoshis.place but with Conway's Game of Life
 * Copyright (C) 2018 Trey Del Bonis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var WORLD_WIDTH = 256;
var WORLD_HEIGHT = WORLD_WIDTH;

const DEFAULT_ZOOM = 2.0;
const ZOOM_SPRINGYNESS = 3;

const CAM_MOVE_SPEED = 250;
const CAM_ZOOM_MULT = 1.05;

var cameraState = null;
var cameraTarget = null;
var viewCanvas = null;

var gWorldState = getStartingWorldCells();
var gEditWindow = null;
var gotFirstWorld = false;
var pendingAdjustments = []; // in case things get misordered

var gSocket = null;
var msgHandlers = {};
var userToken = "notyetimplemented";

var mouse = null;
var keys = {};
var curToolName = null;
var curTool = null;

var userColor = Math.floor(Math.random() * COLORS.length);
var userDraw = []; // list of positions

var pendingDraws = {};

var debug = {
    printTilesRendered: false,
    noRenderGameState: false,
    noRenderCells: false,
    debugCellColors: false,

    // this won't look very good if the world is much larger than 256
    drawMinimap: false
};

function init() {

    console.log("hello!");

    // Set up camera.
    cameraState = {
	x: WORLD_WIDTH / 2,
	y: WORLD_HEIGHT / 2,
	zoom: DEFAULT_ZOOM / 5.0,
    };

    cameraTarget = {
	x: WORLD_WIDTH / 2,
	y: WORLD_HEIGHT / 2,
	zoom: DEFAULT_ZOOM
    };

    // Set up mouse.
    mouse = {
	x: WORLD_WIDTH / 2,
	y: WORLD_HEIGHT / 2
    };

    // Set up game UI rendering.
    cellsCanvas = document.createElement("canvas");
    worldCanvas = document.createElement("canvas");
    screenCanvas = document.createElement("canvas");

    // Controls
    window.onkeydown = handleKeyDown;
    window.onkeyup = handleKeyUp;
    viewCanvas = document.getElementById("game");
    viewCanvas.addEventListener("click", handleClick);
    viewCanvas.addEventListener("mousemove", handleMouseMove);

    // Now actually set up the starting tool.
    switchToTool(defaultTool);

    // Start render procedures.
    updateDisplay();
    updateSatoshiCostDisplay();
    updateUserPen();
    window.requestAnimationFrame(runFrameUpdate)

    // Set up message handlers.  See messages.rs for more info.
    msgHandlers["Alert"] = function(sock, m) { alert(m); };
    msgHandlers["Log"] = function(sock, m) { console.log("remote: " + m); };
    msgHandlers["NewWorldState"] = function(sock, m) { applyNewWorldState(m.world) };
    msgHandlers["UpdateCells"] = function(sock, m) { applyWorldUpdates(m) };
    msgHandlers["UpdateEditWindow"] = function(sock, m) { gEditWindow = m; };
    msgHandlers["Invoice"] = function(sock, m) { handleInvoice(m[0], m[1]); };
    msgHandlers["InvoicePaid"] = function(sock, m) { handleInvoicePaid(m); };

    // Set up connection.
    let socket = new WebSocket(getSocketUrl(), "gameoflight");
    gSocket = socket;
    socket.onopen = function(e) { handleSocketOpen(socket, e); };
    socket.onmessage = function(e) { handleSocketMessage(socket, e); };

    console.log("setup finished!");

}

function switchToTool(name) {

    if (!TOOLS.hasOwnProperty(name)) {
	console.log("Tried to switch to invalid tool: " + name);
	return false;
    }

    console.log("Switching tools: " + name);

    // Invoke the "switch from" handler.
    if (curTool != undefined && curTool != null) {
	curTool.onSwitchFrom(name);
    }

    // Switch out the handler.
    curToolName = name;
    curTool = TOOLS[name];

    // Invoke the init function.
    curTool.onSwitchTo()

    return true;
}

function applyNewWorldState(s) {

    console.log("got new whole world state (t = " + s.tick + ")");
    gotFirstWorld = true;

    // Update the dimensions we know about.
    WORLD_WIDTH = s.dimensions[0];
    WORLD_HEIGHT = s.dimensions[1];

    // Create a new empty world grid.
    let nw = [];
    for (let i = 0; i < WORLD_WIDTH; i++) {
	let nc = [];
	for (let j = 0; j < WORLD_HEIGHT; j++) {
	    nc.push(newBlankTile());
	}
	nw.push(nc);
    }

    let liveCnt = 0;

    // Populate the grid with the tiles from the new state.
    for (let i = 0; i < s.cells.length; i++) {
	let tx = i % WORLD_WIDTH;
	let ty = (i - tx) / WORLD_WIDTH;
	let c = s.cells[i];
	nw[tx][ty] = {
	    live: c.live,
	    color: colorNameToId(c.data.color)
	};
	if (c.data.live) {
	    liveCnt++;
	}
    }

    console.log("found " + liveCnt + "/" + s.cells.length + " live cells in initial world");

    // Now make some tweaks that got sent out of order.
    for (let i = 0; i < pendingAdjustments.length; i++) {
	let updateSet = pendingAdjustments[i];
	for (let j = 0; j < updateSet.length; j++) {
	    applyTileStateUpdate(updateSet[j]);
	}
    }
    console.log("applied " + pendingAdjustments.length + " misordered update messages");

    // Apply it and redraw it.
    gWorldState = nw;
    cellsCanvas.width = WORLD_WIDTH;
    cellsCanvas.height = WORLD_HEIGHT;
    let redrew = updateCellsCanvasInViewport(cameraState);
    if (redrew > 0 && debug.printTilesRendered) {
	console.log("[world reset] redrew " + redrew + "/" + gWorldState.length + " cells due to world reset");
    }

}

function applyWorldUpdates(updatesMsg) {

    let updates = updatesMsg[1];
    let tick = updatesMsg[0];

    // In case messages come out of order, we should queue the updates for later.
    if (!gotFirstWorld) {
	console.log("deferring updates for " + updates.length + " cells (t = " + tick + ")");
	pendingAdjustments.push(updates[1]);
	return;
    }

    let pts = [];

    console.log("applying updates for " + updates.length + " cells (t = " + tick + ")");
    for (let i = 0; i < updates.length; i++) {
	let u = updates[i];
	applyTileStateUpdate(u);
	pts.push({x: u.pos[0], y: u.pos[1]});
    }

    // Redraw cells in the viewport that we changed.
    let redrew = updateCellsCanvasInViewport_selective(cameraState, pts);
    if (redrew > 0 && debug.printTilesRendered) {
	console.log("[world update] redrew " + redrew + "/" + pts.length + " cells due to tick update");
    }

}

function applyTileStateUpdate(update) {
    gWorldState[update.pos[0]][update.pos[1]] = {
	live: update.data.live,
	color: colorNameToId(update.data.color)
    };
}

function colorNameToId(name) {
    // These colors are from the table above.
    if (name == "None") {
	return 0;
    } else if (name == "Red") {
	return 4;
    } else if (name == "Blue") {
	return 1;
    } else if (name == "Green") {
	return 2;
    } else if (name == "Yellow") {
	return 6;
    }
    return 0;
}

function getSocketUrl() {
    let proto = window.location.protocol;
    if (proto == "file:") {
	/*
	 * We're running it directly, let's hope that the webapp server is
	 * running too.  Normal users will never get here.  Don't worry, 7908
	 * isn't exposed publicly on the server.
	 */
	return "ws://localhost:7908";
    } else {
	/*
	 * We're running on a web server.  This looks more complicated but it's
	 * really not.
	 */
	return "ws://" + window.location.hostname + (window.location.port ? ":" + window.location.port : "") + "/api";
    }
}

function handleSocketOpen(sock, e) {
    console.log("Connected, sending handshake");
    gSocket.send(JSON.stringify({
	token: userToken
    }));
}

function handleSocketMessage(sock, e) {
    let msg = JSON.parse(e.data);
    let handle = msgHandlers[msg.type];
    if (handle != null && handle != undefined) {
	handle(sock, msg.body);
    }
}

function handleKeyDown(e) {
    keys[e.key.toLowerCase()] = true;
}

function handleKeyUp(e) {
    keys[e.key.toLowerCase()] = false;
}

function handleClick(e) {
    let wc = convertScreenSpaceToWorldSpace(mouse);
    let tx = Math.floor(wc.x);
    let ty = Math.floor(wc.y);

    if (curTool != null) {
	curTool.onClick(e, tx, ty);
    }
}

function handleMouseMove(e) {

    // Update some fields.
    let br = viewCanvas.getBoundingClientRect();
    mouse.x = e.clientX - br.x;
    mouse.y = e.clientY - br.top;

    // Now we *also* want to call out to the tool
    if (curTool != null) {
	let wc = convertScreenSpaceToWorldSpace(mouse);
	let tx = Math.floor(wc.x);
	let ty = Math.floor(wc.y);

	curTool.onMouseMove(e, tx, ty)
    }
}

function getStartingWorldCells() {
    let w = [];
    for (let i = 0; i < WORLD_HEIGHT; i++) {
	let r = [];
	for (let j = 0; j < WORLD_WIDTH; j++) {
	    let t = newBlankTile();
	    t.live = (i + j) % 7 == 0;
	    t.color = 0;
	    r.push(t);
	}
	w.push(r);
    }
    return w;
}

function newBlankTile() {
    return {
	live: false,
	color: 0
    }
}

function submitPendingCells() {

    // Construct the list of updates, with the full settings.
    let updates = [];
    for (let i = 0; i < userDraw.length; i++) {
	let c = userDraw[i];
	updates.push({
	    x: c.x,
	    y: c.y,
	    live: true, // TODO Make a way to remove cells.
	    data: userColor
	})
    }

    // Now we preemptively open the invoice box, and then send the request off.
    showInvoiceBox("Processing...")
    sendMessageToServer("SubmitTiles", {updates: updates});

    // Also don't forget to clear the cost display.
    updateSatoshiCostDisplay();
}

function convertScreenSpaceToWorldSpace(pos) {
    return {
	x: ((pos.x - lastCanvasWidth / 2) / cameraState.zoom) + cameraState.x,
	y: ((pos.y - lastCanvasHeight / 2) / cameraState.zoom) + cameraState.y
    }
}

function updateSatoshiCostDisplay() {
    let se = document.getElementById("pendingcost");
    se.innerHTML = computeCellSatoshiCost(userDraw.length);
}

/*
 * This is just here in case we have to change it later.  And no, you can't just
 * change this function to make your drawing cost less.  This is just for the UI
 * and you'd only be lying to yourself.
 */
function computeCellSatoshiCost(n) {
    return n * 10;
}
