#![allow(unused)]

use std::fmt::{self, Display, Formatter};
use std::iter::*;
use std::ops::Range;
use std::time;

pub trait TileData: Copy + Eq {
    /// Called when a cell converts from dead to living.
    fn on_to_live(&self, adjs: &[&Tile<Self>]) -> Self;

    /// Called when a cell converts from living to dead.
    fn on_to_dead(&self, adjs: &[&Tile<Self>]) -> Self;
}

impl TileData for () {
    fn on_to_live(&self, adjs: &[&Tile<Self>]) -> Self {
        ()
    }
    fn on_to_dead(&self, adjs: &[&Tile<Self>]) -> Self {
        ()
    }
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Tile<D: TileData> {
    pub live: bool,
    pub last_update: u64,
    pub data: D,
}

impl<D: TileData> PartialEq for Tile<D> {
    fn eq(&self, other: &Self) -> bool {
        self.live == other.live && self.data == other.data
    }
}

impl<D: TileData> Tile<D>
where
    D: Default,
{
    pub fn new_default_empty() -> Tile<D> {
        Tile {
            live: false,
            last_update: 0,
            data: D::default(),
        }
    }
    pub fn new_default_empty_live() -> Tile<D> {
        Tile {
            live: true,
            last_update: 0,
            data: D::default(),
        }
    }
    pub fn new_default_with_state(living: bool) -> Tile<D> {
        Tile {
            live: living,
            last_update: 0,
            data: D::default(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct World<D: TileData> {
    cells: Vec<Tile<D>>,
    dimensions: (usize, usize),
    tick: u64,
}

const NEIGHBOR_OFFSETS: [(isize, isize); 8] = [
    (-1, -1),
    (0, -1),
    (1, -1),
    (-1, 0),
    (1, 0),
    (-1, 1),
    (0, 1),
    (1, 1),
];

impl<D> World<D>
where
    D: TileData + Default,
{
    pub fn new_default((w, h): (usize, usize)) -> World<D> {
        World {
            cells: vec![Tile::new_default_empty(); w * h],
            dimensions: (w, h),
            tick: 0,
        }
    }

    pub fn from_liveness_bools(
        (w, h): (usize, usize),
        tiles: Vec<bool>,
    ) -> Result<World<D>, String> {
        // Quickly error out of the length of the tiles doesn't make sense.
        if tiles.len() != w * h {
            return Err(String::from("len doesn't match dims"));
        }

        Ok(World {
            cells: tiles
                .into_iter()
                .map(Tile::<D>::new_default_with_state)
                .collect(),
            dimensions: (w, h),
            tick: 0,
        })
    }

    pub fn tick(&self) -> u64 {
        self.tick
    }
}

impl<D: TileData> World<D> {
    pub fn cell_at(&self, pos: (usize, usize)) -> Option<&Tile<D>> {
        match cartesean_to_index(self.dimensions, pos) {
            Some(i) => Some(&self.cells[i]),
            None => None,
        }
    }

    pub fn cell_at_mut(&mut self, pos: (usize, usize)) -> Option<&mut Tile<D>> {
        match cartesean_to_index(self.dimensions, pos) {
            Some(i) => Some(&mut self.cells[i]),
            None => None,
        }
    }

    #[inline]
    fn neighbors_to(&self, (x, y): (usize, usize)) -> Vec<&Tile<D>> {
        NEIGHBOR_OFFSETS
            .into_iter()
            .map(|(dx, dy)| self.cell_at(((x as isize + dx) as usize, (y as isize + dy) as usize)))
            .filter(Option::is_some)
            .map(Option::unwrap)
            .collect()
    }

    pub fn set_tile_liveness(&mut self, pos: (usize, usize), live: bool) -> bool {
        match cartesean_to_index(self.dimensions, pos) {
            Some(i) => {
                let oc = self.cells[i];
                self.cells[i] = Tile {
                    live,
                    last_update: oc.last_update,
                    data: if oc.live != live {
                        let adjs = self.neighbors_to(pos);
                        if live {
                            D::on_to_live(&oc.data, adjs.as_slice())
                        } else {
                            D::on_to_dead(&oc.data, adjs.as_slice())
                        }
                    } else {
                        oc.data
                    },
                };
                true
            }
            None => false,
        }
    }

    pub fn set_tile_state_directly(
        &mut self,
        pos: (usize, usize),
        live: bool,
        data: D,
        update_timestamp: bool,
    ) -> bool {
        match cartesean_to_index(self.dimensions, pos) {
            Some(i) => {
                let oc = self.cells[i];
                let c = &mut self.cells[i];
                c.live = live;
                c.data = data;
                if update_timestamp {
                    c.last_update = self.tick;
                }
                true
            }
            None => false,
        }
    }

    pub fn step(&self) -> World<D> {
        // First compute the state of each of the tiles.
        let next_tiles: Vec<Tile<D>> = (0..self.cells.len())
            .into_iter()
            // The unwrap is fine because we never pass in an OOB thing.  Same for the index.
            .map(|i| {
                (
                    index_to_cartesean(self.dimensions, i).unwrap(),
                    &self.cells[i],
                )
            })
            .map(|(p, s)| {
                let adjs: Vec<_> = self
                    .neighbors_to(p)
                    .into_iter()
                    .filter(|t| t.live)
                    .collect();
                compute_tile_next_step(adjs.as_slice(), s, self.tick + 1)
            })
            .collect();

        // Then actually assemble the new world.
        World {
            cells: next_tiles,
            dimensions: self.dimensions,
            tick: self.tick + 1,
        }
    }

    pub fn cells(&self) -> &Vec<Tile<D>> {
        &self.cells
    }

    pub fn dims(&self) -> (usize, usize) {
        self.dimensions
    }

    /// Computes the differences needed to convert a particlar state to this state.
    pub fn diffs_from_state<'w>(
        &'w self,
        other: &World<D>,
    ) -> Result<Vec<(usize, usize, &'w Tile<D>)>, ()> {
        // Quick dimensions check first, this function only makes sense for same-shape worlds.
        if self.dimensions != other.dimensions {
            return Err(());
        }

        // Go through all the cells in the two lists and figure out where they're different.
        let mut diffs = Vec::new();
        for (i, (a, b)) in self.cells.iter().zip(other.cells.iter()).enumerate() {
            if a.live != b.live || a.data != b.data {
                let (x, y) = index_to_cartesean(self.dimensions, i).unwrap();
                diffs.push((x, y, a));
            }
        }

        Ok(diffs)
    }

    pub fn num_live_cells(&self) -> u64 {
        self.cells
            .iter()
            .fold(0, |a, e| a + if e.live { 1 } else { 0 })
    }
}

#[inline]
fn compute_tile_next_step<D>(adjacents: &[&Tile<D>], subject: &Tile<D>, tick_for: u64) -> Tile<D>
where
    D: TileData,
{
    let will_live = adjacents.len() == 3 || (subject.live && adjacents.len() == 2);
    Tile {
        live: will_live,
        last_update: if will_live != subject.live {
            tick_for
        } else {
            subject.last_update
        },
        data: if will_live != subject.live {
            if will_live {
                D::on_to_live(&subject.data, adjacents)
            } else {
                D::on_to_dead(&subject.data, adjacents)
            }
        } else {
            subject.data
        },
    }
}

#[inline]
fn cartesean_to_index((w, h): (usize, usize), (x, y): (usize, usize)) -> Option<usize> {
    if x >= w || y >= h {
        None
    } else {
        Some(w * y + x)
    }
}

#[inline]
pub fn index_to_cartesean((w, h): (usize, usize), i: usize) -> Option<(usize, usize)> {
    if i >= w * h {
        None
    } else {
        let x = i % w;
        let y = i / w;
        Some((x, y))
    }
}

impl<D: TileData> Display for World<D> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        let world_box: String = vec!['='; self.dimensions.0].iter().collect();
        f.write_str(world_box.as_ref());
        f.write_str("\n");
        for l in 0..self.dimensions.1 {
            // for each line
            let mut s = String::new();
            for c in 0..self.dimensions.0 {
                match self.cell_at((c, l)) {
                    Some(c) => s.push(if c.live { '#' } else { ' ' }),
                    None => return Ok(()), // TODO Make this not suck.
                }
            }
            s.push('\n');
            f.write_str(s.as_str());
        }
        f.write_str(world_box.as_ref());
        Ok(())
    }
}

#[cfg(test)]
mod test {

    extern crate test;

    use self::test::Bencher;
    use super::*;

    #[test]
    fn test_i2c_1() {
        assert_eq!(Some((5, 0)), index_to_cartesean((20, 1), 5))
    }

    #[test]
    fn test_c2i_1() {
        assert_eq!(Some(7), cartesean_to_index((3, 5), (1, 2)))
    }

    #[test]
    fn debug_world_deser() {
        // This is a glider.  Run with --nocapture.
        let tiles = vec![
            false, false, false, false, false, false, false, true, false, false, false, false,
            false, true, false, false, true, true, true, false, false, false, false, false, false,
        ];

        println!("{}", World::from_bools((5, 5), tiles).unwrap());
    }

    #[bench]
    fn bench_step(b: &mut Bencher) {
        let pat = [
            false, true, false, true, true, false, true, false, true, false, true, true, false,
        ];

        let wb = (0..1000000)
            .into_iter()
            .map(|i| pat[i % pat.len()])
            .collect();

        let mut w = World::from_bools((1000, 1000), wb).unwrap();
        b.iter(|| w = w.step());
    }

}
