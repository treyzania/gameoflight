use uuid::Uuid;

use conway;

pub type World = conway::world::World<TileData>;

#[derive(Copy, Clone, Default, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct TileData {
    pub color: TileColor,
    pub updater: Option<Uuid>,
}

impl conway::world::TileData for TileData {
    fn on_to_dead(&self, adjs: &[&conway::world::Tile<Self>]) -> Self {
        TileData {
            color: self.color,
            updater: None,
        }
    }
    fn on_to_live(&self, adjs: &[&conway::world::Tile<Self>]) -> Self {
        // Should do something about this to copy updater data.
        *self
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub enum TileColor {
    None,
    Red,
    Blue,
    Green,
    Yellow,
}

impl Default for TileColor {
    fn default() -> Self {
        TileColor::None
    }
}

/// A set of tile states to set, usually for an invoice.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct WriteBatch {
    pub writes: Vec<TileWrite>,
}

#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct TileWrite {
    pub pos: (usize, usize),
    pub live: bool,
    pub data: TileData,
}

impl WriteBatch {
    pub fn apply_to(&self, world: &mut World) {
        for w in &self.writes {
            world.set_tile_state_directly(w.pos, w.live, w.data, false);
        }
    }

    pub fn to_json(&self) -> Box<[u8]> {
        Box::from(serde_json::to_string(self).unwrap().as_bytes())
    }

    pub fn from_json(buf: &[u8]) -> Result<Self, String> {
        serde_json::from_slice(&buf).map_err(|e| format!("{:?}", e))
    }
}
