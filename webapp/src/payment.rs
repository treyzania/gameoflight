use std::path::PathBuf;
use std::sync::{Arc, Mutex};

use clightningrpc;
use clightningrpc::lightningrpc::LightningRPC;
use futures::{
    future::{lazy, poll_fn},
    prelude::*,
    sync::{mpsc, oneshot},
};
use rand::{self, prelude::*};
use tokio_threadpool::blocking;

use crate::util;

pub struct CLNodeSock {
    rpc_path: PathBuf,
}

impl CLNodeSock {
    pub fn new(rpc_path: PathBuf) -> CLNodeSock {
        CLNodeSock { rpc_path }
    }

    /// Creates a future that will resolve to the BOLT-11 of the invoice generated.
    pub fn create_new_invoice(
        &self,
        msat: i64,
        label: String,
        desc: String,
    ) -> impl Future<Item = String, Error = NodeError> {
        let (c, p): (
            oneshot::Sender<Result<String, NodeError>>,
            oneshot::Receiver<Result<String, NodeError>>,
        ) = oneshot::channel();

        let cell = Arc::new(Mutex::new(Some(c)));
        let rpc_path = self.rpc_path.clone();

        // This is way more complicated than it needs to be.
        // TODO Make this less gross.
        let invoice_fut = poll_fn({
            move || {
                blocking({
                    let desc = desc.clone();
                    let label = label.clone();
                    let rpc_path = rpc_path.clone();
                    let c = cell
                        .as_ref()
                        .lock()
                        .expect("mkinvoicelock")
                        .take()
                        .expect("mkinvoice");
                    move || {
                        let mut rpc = LightningRPC::new(&rpc_path);
                        let res = rpc
                            .invoice(msat, label, desc, None)
                            .map(|i| i.bolt11)
                            .map_err(|e| NodeError::Server(format!("{:?}", e)));
                        c.send(res);
                    }
                })
            }
        })
        .map_err(|_| ());

        util::spawn_task(invoice_fut.map_err(|_| ()), "mkinvoice", true); // TODO Make not noisy
        p.map_err(|_| NodeError::Unknown).and_then(|r| r)
    }

    // Spawns a background task that waits for new invoice payments, returns a Stream of the payment labels.
    pub fn spawn_invoice_processor(
        &self,
        last_index: Option<i64>,
    ) -> impl Stream<Item = String, Error = NodeError> {
        let (tx_labels, rx_labels) = mpsc::channel(1);

        let rpc_path = self.rpc_path.clone();

        // This is way more complicated than it needs to be.
        // TODO Make this less gross.
        let recv_fut = poll_fn({
            move || {
                blocking({
                    let rpc_path = rpc_path.clone();
                    let mut tx = tx_labels.clone().wait();
                    move || {
                        let mut rpc = LightningRPC::new(&rpc_path);
                        let mut last_index = last_index;

                        loop {
                            match rpc.waitanyinvoice(last_index) {
                                Ok(payment) => {
                                    // Update the payment number for the next one.
                                    last_index = payment.pay_index;

                                    // Send the invoice to the sink.
                                    tx.send(payment.label.clone())
                                        .map_err(|e| {
                                            format!(
                                                "error handling newly recieved payment: {:?}",
                                                e
                                            )
                                        })
                                        .unwrap();
                                }
                                Err(e) => {
                                    println!("[payproc] error: {:?}", e);
                                    break;
                                }
                            }
                        }
                    }
                })
            }
        });

        // Spawn the task to actually receive them and return the payments stream.
        util::spawn_task(recv_fut.map_err(|_| ()), "procpayments", true); // TODO Make not noisy
        rx_labels.map_err(|e| NodeError::Server(format!("{:?}", e)))
    }
}

#[derive(Clone, Debug)]
pub enum NodeError {
    Server(String),
    Unknown,
}

pub fn new_rand_invoice_label() -> String {
    let mut rng = rand::thread_rng();
    let mut buf = [0; 8];
    rng.fill_bytes(&mut buf);
    buf.iter().map(|b| format!("{:02x}", b)).collect::<String>()
}
