#![allow(unused)]
#![feature(integer_atomics)]

#[macro_use]
extern crate clap;
extern crate conway;
extern crate serde;
extern crate websocket;
#[macro_use]
extern crate serde_derive;
extern crate clightningrpc;
extern crate futures;
extern crate rand;
extern crate serde_json;
extern crate sled;
extern crate tokio;
extern crate tokio_timer;
extern crate uuid;

use std::env;
use std::fmt::Debug;
use std::iter::*;
use std::path::PathBuf;
use std::sync::atomic::AtomicU64;
use std::sync::*;
use std::thread;

use clightningrpc::LightningRPC;
use futures::{Future, Sink, Stream};
use tokio::{net, runtime};
use websocket::message::{Message, OwnedMessage};
use websocket::r#async::Server;
use websocket::server::InvalidConnection;

use conway::world;

mod db;
mod game;
mod messages;
mod payment;
mod util;
mod worldstate;

const PROTO_NAME: &'static str = "gameoflight";
const GAME_TICK_MILLIS: u64 = 2500;
const WORLD_SIZE: usize = 256;

fn main() {
    let matches = clap_app!(gameoflightinst =>
        (version: "0.1.0")
        (author: "treyzania <treyzania@gmail.com>")
        (about: "The Conway's Game of Life instance game server.")
        (@arg wsport: --port -p +takes_value "Port to host the websocket server on.  Default: 7908")
        (@arg clsock: --clsocket -s +takes_value "Socket path for c-lightning.  Default: default")
        (@arg testnet: --testnet "Switch to using testnet stuff."))
    .get_matches();

    let ws_port: u16 = matches
        .value_of("wsport")
        .unwrap_or("7908")
        .parse()
        .unwrap();

    // Figure out the full path to the socket for c-lightning.
    #[allow(deprecated)]
    let cl_sock = matches
        .value_of("clsock")
        .map(PathBuf::from)
        .map(|r| env::current_dir().unwrap().join(r))
        .unwrap_or(env::home_dir().unwrap().join(".lightning/lightning-rpc"));
    println!("c-lightning socket: {}", cl_sock.display());

    let testnet_huh: bool = matches
        .value_of("testnet")
        .unwrap_or("false")
        .parse()
        .unwrap(); // testnet?

    // Load the world.
    let start_world = load_world();

    // Open database.
    let db_cfg = sled::ConfigBuilder::new()
        .path(PathBuf::from("invoicedb"))
        .build();
    let db = sled::Db::start(db_cfg).expect("loaddb");
    let invoices_tree = db.open_tree("invoices").expect("dbinvoicetree");
    let invoices_tree_wrapper = db::AsyncKvStore::new(invoices_tree);

    // Set up the tokio env.
    let mut rt = runtime::Builder::new().build().unwrap();

    // Start the server.
    game::start_server(cl_sock, invoices_tree_wrapper, ws_port, start_world);

    // Wait for termination.
    rt.shutdown_on_idle();
}

fn load_world() -> worldstate::World {
    // TODO Make this actually load a file.
    let mut w = worldstate::World::new_default((WORLD_SIZE, WORLD_SIZE));
    make_r_pentomino(&mut w, (WORLD_SIZE / 2, WORLD_SIZE / 2));
    w
}

fn make_r_pentomino(world: &mut worldstate::World, (x, y): (usize, usize)) {
    if !(world.set_tile_liveness((x, y), true)
        && world.set_tile_liveness((x - 1, y), true)
        && world.set_tile_liveness((x, y - 1), true)
        && world.set_tile_liveness((x + 1, y - 1), true)
        && world.set_tile_liveness((x, y + 1), true))
    {
        panic!("error initing R-pentomino world");
    }
}
