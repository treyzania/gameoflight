use futures::prelude::*;

pub fn spawn_task<F, I, E>(f: F, label: &'static str, noisy: bool)
where
    F: Future<Item = I, Error = E> + Send + 'static,
    E: ::std::fmt::Debug,
{
    tokio::spawn(
        f.map(move |_| {
            if noisy {
                println!("[{} exited]", label);
            }
            ()
        })
        .map_err(move |e| println!("{}: error: {:?}", label, e)),
    );
}
