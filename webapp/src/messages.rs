use std::sync::*;

use serde_json;

use conway::world;

use crate::worldstate;

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "type", content = "body")]
pub enum NetMessage {
    Alert(String),
    Log(String),
    NewWorldState(NewWorldStateMessage),
    UpdateCells(u64, Vec<UpdateCellMessage>),
    RequestEditWindow,
    SubmitTiles(SubmitTilesMessage),
    Invoice(String, String), // ("id", "body")
    InvoicePaid(String),     // "id"
}

impl NetMessage {
    pub fn to_string(&self) -> String {
        serde_json::to_string(self).unwrap()
    }

    pub fn from_string(m: &String) -> Result<Self, String> {
        // FIXME Make errors smarter.
        serde_json::from_str(m.as_ref()).map_err(|e| format!("{:?}", e))
    }

    pub fn new_alert_msg(txt: &str) -> NetMessage {
        NetMessage::Alert(String::from(txt))
    }

    pub fn new_log_msg(txt: &str) -> NetMessage {
        NetMessage::Log(String::from(txt))
    }

    pub fn new_world_state_message(w: Arc<worldstate::World>) -> NetMessage {
        NetMessage::NewWorldState(NewWorldStateMessage { world: w })
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct NewWorldStateMessage {
    world: Arc<worldstate::World>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UpdateCellMessage {
    pub pos: (usize, usize),
    pub data: world::Tile<worldstate::TileData>,
}

impl UpdateCellMessage {
    pub fn msg_from_diffs(
        tick: u64,
        diffs: &[(usize, usize, &world::Tile<worldstate::TileData>)],
    ) -> NetMessage {
        let items = diffs
            .iter()
            .map(|(x, y, t)| UpdateCellMessage {
                pos: (*x, *y),
                data: **t, // lol double-deref
            })
            .collect();
        NetMessage::UpdateCells(tick, items)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SubmitTilesMessage {
    pub updates: Vec<TileSubmission>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TileSubmission {
    pub x: usize,
    pub y: usize,
    pub live: bool,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct TileState {
    pub x: usize,
    pub y: usize,
    pub live: bool,
    pub data: worldstate::TileData,
}

impl From<worldstate::TileWrite> for TileState {
    fn from(f: worldstate::TileWrite) -> Self {
        TileState {
            x: f.pos.0,
            y: f.pos.1,
            live: f.live,
            data: f.data,
        }
    }
}
