use std::collections::*;
use std::error;
use std::net::SocketAddr;
use std::path::PathBuf;
use std::sync::{Arc, Mutex, RwLock};
use std::time;

use futures::{future, stream, Future, Sink, Stream};
use tokio::{
    net,
    prelude::*,
    runtime::*,
    sync::{lock, mpsc, watch},
    timer,
};
use uuid::Uuid;
use websocket::client::r#async::*;
use websocket::codec::ws::MessageCodec;
use websocket::message::OwnedMessage;
use websocket::r#async::{server::upgrade::IntoWs, Server};
use websocket::server::InvalidConnection;
use websocket::WebSocketError;

use conway;

use crate::db;
use crate::messages;
use crate::payment;
use crate::util::spawn_task;
use crate::worldstate;

#[derive(Clone)]
struct PendingPacket {
    client_uuid: Uuid,
    client: Arc<GameClient>,
    msg: messages::NetMessage,
}

struct Game {
    cl_node: payment::CLNodeSock,
    invoice_db: db::AsyncKvStore,

    clients: RwLock<HashMap<Uuid, Arc<GameClient>>>,
    inbound_msgs: mpsc::UnboundedSender<PendingPacket>,

    pending_writes: Mutex<Vec<worldstate::WriteBatch>>,
    cur_world_state: RwLock<Arc<worldstate::World>>,
}

impl Game {
    fn queue_send_all_msg(&self, m: messages::NetMessage) -> impl Future {
        let clients: Vec<Arc<GameClient>> =
            self.clients.read().unwrap().values().cloned().collect();
        future::join_all(clients.into_iter().map(move |c| c.queue_msg(m.clone())))
    }
}

const PROTO_NAME: &'static str = "gameoflight";

const SERVER_BROKE_ERROR_MSG: &'static str = "The server broke, message @trey:foobar.style on Matrix or @treyzania#7744 on Discord about it.";

/*
 * A lot of this connection code was indirectly borrowed from:
 * https://github.com/websockets-rs/rust-websocket/blob/master/examples/async-server.rs
 * I based it on code that I wrote that was taken from ^that.
 */

pub fn start_server(
    cl_node_socket: PathBuf,
    invoice_db: db::AsyncKvStore,
    port: u16,
    init_world: worldstate::World,
) -> Result<(), Box<dyn error::Error>> {
    let addr = format!("0.0.0.0:{}", port).parse::<SocketAddr>()?;

    let (recved_msgs_tx, inbound_queue_rx) = mpsc::unbounded_channel();

    let init_live = init_world.num_live_cells();

    let game = Arc::new(Game {
        cl_node: payment::CLNodeSock::new(cl_node_socket),
        invoice_db: invoice_db,
        clients: RwLock::new(HashMap::new()),
        inbound_msgs: recved_msgs_tx,
        pending_writes: Mutex::new(Vec::new()),
        cur_world_state: RwLock::new(Arc::new(init_world)),
    });

    println!(
        "[init] there are {} live tiles in the starting world",
        init_live
    );

    // Set up the payment stream.
    let last_pay_index = None; // FIXME This isn't right.
    let pay_fut = game
        .cl_node
        .spawn_invoice_processor(last_pay_index)
        .for_each({
            let game = game.clone();
            move |lbl| {
                process_invoice_paid(game.clone(), &lbl);
                // TODO Send a message to the client that they paid it.  Somehow.
                Ok(())
            }
        })
        .map_err(|e| println!("error processing payments: {:?}", e));

    // Set up the inbound message processing.
    let recv_proc_fut = inbound_queue_rx
        .map_err(|e| format!("proc msg err: {}", e))
        .for_each({
            let game = game.clone();
            move |p| {
                process_message(game.clone(), &p)
                    .map_err(|e| {
                        // Print the error.
                        println!("error processing packet from {}: {}", p.client_uuid, e);

                        // Let the client know about the error, hopefully.
                        let err_reply = messages::NetMessage::new_log_msg(&format!("error: {}", e));
                        spawn_task(
                            p.client.queue_msg(err_reply).map_err(|e| {
                                println!("SEVERE: sending error response failed");
                                ()
                            }),
                            "errorreply",
                            true,
                        );

                        // Let the executor know this task failed.
                        e
                    })
                    .map(|_| ())
            }
        })
        .map(|_| ())
        .map_err(|e| println!("error: {}", e));

    /// Handle crap for setting up crap.
    let ticks_fut = create_sim_task(game.clone()).map(|_| ()).map_err({
        let game = game.clone();
        |e| {
            println!("error ticking, aborting!!!");

            // Send a message to everyone that it broken.
            game.queue_send_all_msg(messages::NetMessage::Alert(String::from(
                SERVER_BROKE_ERROR_MSG,
            )));
        }
    });

    // Actually start the websocket thing.
    let serv = Server::bind(&addr, &::tokio::reactor::Handle::default()).unwrap();
    let accepting_fut = serv
        .incoming()
        .map_err(|InvalidConnection { error, .. }| error)
        .zip(stream::repeat(game)) // better way to do this?
        .for_each(move |((upgrade, addr), game)| {
            println!("new connection from: {}", addr);
            let accept_fut =
                upgrade
                    .use_protocol(PROTO_NAME)
                    .accept()
                    .and_then(move |(client, _)| {
                        let conn_init = process_connection(client, addr, game);
                        spawn_task(conn_init, "conninit", true);
                        Ok(())
                    });
            spawn_task(accept_fut, "connaccept", true);
            Ok(())
        });

    let bootstrap_fut = future::lazy(move || {
        spawn_task(ticks_fut, "gametick", true);
        spawn_task(recv_proc_fut, "procmsg", true);
        spawn_task(pay_fut, "payproc", true);

        // This is the one we care about finishing.
        accepting_fut
            .map(|_| println!("closed connections task"))
            .map_err(|e| println!("error accepting connections: {:?}", e))
    });

    // Actually run it and then wait until everything is done.
    println!("Starting listening task...");
    tokio::run(bootstrap_fut);

    return Ok(());
}

enum OutboundAction {
    Msg(messages::NetMessage),
    Close,
}

struct GameClient {
    addr: SocketAddr,
    outbound_ch: mpsc::UnboundedSender<OutboundAction>,
}

impl GameClient {
    fn queue_msg(&self, m: messages::NetMessage) -> impl Future<Item = (), Error = ()> {
        self.outbound_ch
            .clone() // is this a good idea?
            .send(OutboundAction::Msg(m))
            .map(|_| ())
            .map_err(|_| ())
    }
    fn queue_close_connection(&self) -> impl Future<Item = (), Error = ()> {
        self.outbound_ch
            .clone() // is this a good idea?
            .send(OutboundAction::Close)
            .map(|_| ())
            .map_err(|_| ())
    }
}

#[derive(Clone, Debug, Deserialize)]
struct ClientHandshake {
    token: String,
}

fn process_connection<S, E>(
    client_io: S,
    addr: SocketAddr,
    game: Arc<Game>,
) -> impl Future<Item = (Uuid, Arc<GameClient>), Error = String>
where
    S: Sink<SinkItem = OwnedMessage, SinkError = E>
        + Stream<Item = OwnedMessage, Error = E>
        + Send
        + 'static,
    E: ::std::fmt::Debug,
{
    let (sink, stream) = client_io.split();

    let (outbound_ch, send_queue) = mpsc::unbounded_channel();
    let client = Arc::new(GameClient { addr, outbound_ch });

    // Future for sending outbound messages.
    let send_fut = send_queue
        // There's probs a better way to do these two combinators.
        .take_while(|m| match m {
            OutboundAction::Msg(m) => Ok(true),
            _ => Ok(false),
        })
        .filter_map(|m| match m {
            OutboundAction::Msg(m) => Some(m),
            _ => None,
        })
        .map(|m| OwnedMessage::Text(serde_json::to_string(&m).expect("serialize msg")))
        .map_err(|e| format!("queue err: {:?}", e))
        .forward(sink.sink_map_err(|e| format!("write err: {:?}", e)));

    // Set up the task for sending queues outbound messages.
    stream
        .into_future()
        .map_err(|e| format!("read err, this shouldn't happen!"))
        // Handle the handshake so we can actually init the game client.
        .and_then(move |(first_msg, stream)| match first_msg {
            Some(OwnedMessage::Text(json)) => {
                // Try to parse the handshake.
                match serde_json::from_str::<ClientHandshake>(&json) {
                    Ok(handshake) => {
                        // Generate the UUID from the client's provided token.
                        let uuid = to_uuid_from_token(&handshake.token);
                        println!("Connection from {} has UUID {}", addr, uuid);

                        // Send the current world state to the remote.
                        {
                            let world = game.cur_world_state.read().unwrap().clone();
                            println!(
                                "[client init] client's starting world has {} live cells",
                                world.num_live_cells()
                            );
                            let init_msg = messages::NetMessage::new_world_state_message(world);

                            let game = game.clone(); // reasons
                            let client = client.clone();

                            // Here we set up a task to queue the initial world
                            // message, *then* insert the client into the map.
                            // This would get waaaay simpler (and more correct)
                            // if we had async/await syntax, but here we are.
                            // TODO Make this not noisy.
                            spawn_task(
                                client
                                    .clone()
                                    .queue_msg(init_msg)
                                    .and_then(move |_| {
                                        // Insert the client.
                                        game.clients.write().unwrap().insert(uuid.clone(), client);
                                        Ok(())
                                    })
                                    .map_err(|_| ()),
                                "clientinitworld",
                                true,
                            );
                        }

                        let inbound_q = game.inbound_msgs.clone();

                        let recv_fut = stream
                            .fuse()
                            .map_err(|e| format!("recv err: {:?}", e))
                            .and_then(|p| match p {
                                OwnedMessage::Text(json) => {
                                    serde_json::from_str::<messages::NetMessage>(&json)
                                        .map_err(|e| format!("packet parse err: {}", e))
                                }
                                _ => Err(format!("bad packet type")),
                            })
                            .map({
                                let c = client.clone();
                                move |m| PendingPacket {
                                    client_uuid: uuid,
                                    client: c.clone(),
                                    msg: m,
                                }
                            })
                            .forward(inbound_q.sink_map_err(|e| format!("proc msg err: {:?}", e)))
                            .and_then(move |_| {
                                // Take the client out of the set now because it's gone.
                                // This .clone() here is because this closure needs ownership, and
                                // that's apparently the simplest place to put it.
                                println!("Client {} disconnected", uuid);
                                game.clients.write().unwrap().remove(&uuid.clone());
                                Ok(())
                            });

                        // Spawn the connection futures, we're ready now.
                        spawn_task(send_fut, "sendpacket", true);
                        spawn_task(recv_fut, "recvpacket", true); // get a message on shutdown

                        // Return the new client.
                        Ok((uuid, client))
                    }
                    Err(e) => Err(format!("handshake parse err: {:?}", e)),
                }
            }
            Some(OwnedMessage::Close(_)) => Err(format!("closed with no handshake")),
            Some(_) => Err(format!("invalid handshake type")),
            None => Err(format!("no handshake recieved, ignoring")),
        })
}

fn to_uuid_from_token(token: &str) -> Uuid {
    Uuid::new_v5(&Uuid::NAMESPACE_URL, token.as_bytes())
}

const MSAT_PER_TILE: i64 = 5000;

const INVOICE_ERROR_STRING: &'static str = "There was a problem processing your submission.\nContact me at @trey:foobar.style on Matrix or @treyzania@#7744 on Discord.";

fn process_message(game: Arc<Game>, pkt: &PendingPacket) -> Result<(), String> {
    use messages::NetMessage::*;

    println!("Connection {} sent us {:?}", pkt.client_uuid, pkt.msg);

    match &pkt.msg {
        SubmitTiles(m) => {
            let color = compute_user_color(&pkt.client_uuid);
            let batch = worldstate::WriteBatch {
                writes: m
                    .updates
                    .iter()
                    .map(|s| worldstate::TileWrite {
                        pos: (s.x, s.y),
                        live: s.live,
                        data: worldstate::TileData {
                            color: color,
                            updater: Some(pkt.client_uuid),
                        },
                    })
                    .collect(),
            };

            let batch_bytes = batch.to_json();

            let num_tiles = batch.writes.len();
            let cost = num_tiles as i64 * MSAT_PER_TILE;
            let label = payment::new_rand_invoice_label();
            let desc = format!("setting state of {} tiles", num_tiles);

            let client = pkt.client.clone();

            // Create the invoice and save data to disk.
            let invoice_fut = game
                .as_ref()
                .cl_node
                .create_new_invoice(cost, label.clone(), desc)
                .then({
                    let client = client.clone();
                    move |r| match r {
                        Ok(bolt11) => {
                            let label = label.clone();
                            let prefix: String = bolt11.chars().take(10).collect();
                            println!(
                                "[submission] new invoice {} for {} tiles created as {}",
                                label, num_tiles, prefix
                            );

                            client.queue_msg(messages::NetMessage::Invoice(label, bolt11));
                            Ok(())
                        }
                        Err(e) => {
                            println!("error processing invoice {}: {:?}", label, e);

                            // Send message to people saying it broke.
                            client.queue_msg(messages::NetMessage::Alert(format!(
                                "{}\n\nInclude this invoice label: {}",
                                INVOICE_ERROR_STRING, label
                            )));
                            Err(e)
                        }
                    }
                })
                .map_err(|e| println!("error sending invoice!"));

            // Save it in a separate future.
            let save_fut = game
                .as_ref()
                .invoice_db
                .set(Box::from(label.as_bytes()), batch_bytes)
                .map_err(|e| println!("error saving submission data: {:?}", e));

            spawn_task(invoice_fut.map(|_| ()), "createinvoice", true);
            spawn_task(save_fut.map(|_| ()), "savesubmission", true);
        }
        _ => {
            println!("WARN: actually, we don't know what to do with that");
        }
    }

    Ok(())
}

fn compute_user_color(uuid: &Uuid) -> worldstate::TileColor {
    use worldstate::TileColor::*;
    match uuid.as_fields().0 & 0x03 {
        0x00 => Red,
        0x01 => Green,
        0x02 => Blue,
        0x03 => Yellow,
        _ => unreachable!(),
    }
}

const GAME_BEGIN_DELAY_MILLIS: u64 = 5000;
const GAME_TICK_MILLIS: u64 = 2000;

fn create_sim_task(game: Arc<Game>) -> impl Future {
    let start = time::Instant::now() + time::Duration::from_millis(GAME_BEGIN_DELAY_MILLIS);
    let period = time::Duration::from_millis(GAME_TICK_MILLIS);
    let ticks = timer::Interval::new(start, period);

    // Actually do the behavior.
    ticks.for_each(move |_| {
        let cur_world = game.cur_world_state.read().unwrap().clone();

        // First we just dumbly compute the next step in the world sim, timing it.
        // Should we do this in a threadpool blocking future perhaps?
        let start = time::Instant::now();
        let mut next_world = cur_world.step();
        let dur = start.elapsed();

        // Now we have to apply the writes that have been confirmed, this
        // happens now otherwise we'll only see the results from after 1 tick
        // has passed, not what they actually wrote.
        // Also the anonymous scope is for killing the lock sooner.
        {
            let mut writes = game.pending_writes.lock().unwrap();
            if writes.len() != 0 {
                for w in &*writes {
                    w.apply_to(&mut next_world);
                }
                writes.clear();
            }
        }

        // Calculate the number of live cells, for debugging.
        let num_live = next_world.num_live_cells();

        // Compute the changes that we have to send to users and pack it into a
        // message.
        let diffs = next_world.diffs_from_state(cur_world.as_ref()).unwrap();
        let update_msg =
            messages::UpdateCellMessage::msg_from_diffs(next_world.tick(), diffs.as_slice());

        println!(
            "[sim] computed new world state in {} ms, {} changes with now {} live cells",
            dur.subsec_millis() as u64 + 1000 * dur.as_secs(),
            diffs.len(),
            num_live
        );

        // Now, at the same time, send the message to all the users and update
        // the stored world.
        // Same situation with the scope.
        {
            let mut world_lock = game.cur_world_state.write().unwrap();
            let clients_lock = game.clients.read().unwrap();

            // Send the messages.
            // This doesn't use the queue_send_all_msg because we want the lock
            // for it and the world to be taken at the same time.
            for c in clients_lock.values() {
                spawn_task(
                    c.queue_msg(update_msg.clone()).map_err(|_| ()),
                    "sendupdate",
                    false,
                );
            }

            // Now we can update the new world.
            *world_lock = Arc::new(next_world);
        }

        Ok(())
    })
}

fn process_invoice_paid(game: Arc<Game>, label: &String) {
    let g2 = game.clone();
    println!("[pay] invoice {} paid!", label);

    let bytes: Box<[u8]> = Box::from(label.as_bytes());
    let load_fut = game
        .invoice_db
        .get(bytes.clone())
        .map_err(|e| println!("error loading invoice: {:?}", e))
        .and_then(move |raw| {
            if raw.is_some() {
                let sub = worldstate::WriteBatch::from_json(raw.unwrap().as_ref())
                    .expect("parse submission");
                let mut pending = g2.pending_writes.lock().unwrap();
                pending.push(sub);
            } else {
                println!("invoice not found, was it even written?");
            }

            // Now clear the submission.
            g2.invoice_db
                .del(bytes)
                .map_err(|e| println!("error clearning processed submission: {:?}", e))
        });

    // TODO Make this not noisy.
    spawn_task(load_fut, "procpayment", true);
}
