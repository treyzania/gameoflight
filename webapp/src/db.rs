use std::sync::{Arc, Mutex};

use futures::{
    future::{lazy, poll_fn},
    prelude::*,
    sync::oneshot,
};
use sled::{self, Tree};
use tokio_threadpool::blocking;

use crate::util;

pub struct AsyncKvStore {
    sled_tree: Arc<Tree>,
}

#[derive(Debug)]
pub enum DbError {
    Unsupported,
    Severe,
    Unknown,
}

impl From<sled::Error> for DbError {
    fn from(f: sled::Error) -> Self {
        match f {
            sled::Error::Unsupported(_) => DbError::Unsupported,
            _ => DbError::Severe,
        }
    }
}

impl AsyncKvStore {
    pub fn new(sled_tree: Arc<Tree>) -> AsyncKvStore {
        AsyncKvStore { sled_tree }
    }

    pub fn set(
        &self,
        key: Box<[u8]>,
        val: Box<[u8]>,
    ) -> impl Future<Item = Option<Arc<[u8]>>, Error = DbError> {
        let t = self.sled_tree.clone();

        let (c, p): (
            oneshot::Sender<Option<Arc<[u8]>>>,
            oneshot::Receiver<Option<Arc<[u8]>>>,
        ) = oneshot::channel();

        let cell = Arc::new(Mutex::new(Some(c)));

        // This is way more complicated than it needs to be.
        // TODO Make this less gross.
        let set_fut = poll_fn({
            move || {
                blocking({
                    let t = t.clone();
                    let k = key.clone();
                    let v = val.clone();
                    let c = cell
                        .as_ref()
                        .lock()
                        .expect("dbcompletionlock")
                        .take()
                        .expect("dbcompletion");
                    move || {
                        match t.set(k.as_ref(), v) {
                            Ok(Some(iv)) => {
                                let data: Arc<_> = iv.into();
                                c.send(Some(data));
                                Ok(())
                            }
                            Ok(None) => {
                                c.send(None);
                                Ok(())
                            }
                            Err(e) => {
                                println!("db error: {}", e);
                                Err(())
                            }
                        };
                    }
                })
            }
        });

        util::spawn_task(set_fut.map_err(|_| ()), "dbset", true); // TODO Make not noisy
        p.map_err(|_| DbError::Unknown)
    }

    pub fn get(&self, key: Box<[u8]>) -> impl Future<Item = Option<Arc<[u8]>>, Error = DbError> {
        let t = self.sled_tree.clone();

        let (c, p): (
            oneshot::Sender<Option<Arc<[u8]>>>,
            oneshot::Receiver<Option<Arc<[u8]>>>,
        ) = oneshot::channel();

        let cell = Arc::new(Mutex::new(Some(c)));

        // This is way more complicated than it needs to be.
        // TODO Make this less gross.
        let set_fut = poll_fn({
            move || {
                blocking({
                    let t = t.clone();
                    let k = key.clone();
                    let c = cell
                        .as_ref()
                        .lock()
                        .expect("dbcompletionlock")
                        .take()
                        .expect("dbcompletion");
                    move || {
                        match t.get(k.as_ref()) {
                            Ok(Some(iv)) => {
                                let data: Arc<_> = iv.into();
                                c.send(Some(data));
                                Ok(())
                            }
                            Ok(None) => {
                                c.send(None);
                                Ok(())
                            }
                            Err(e) => {
                                println!("db error: {}", e);
                                Err(())
                            }
                        };
                    }
                })
            }
        });

        util::spawn_task(set_fut.map_err(|_| ()), "dbget", true); // TODO Make not noisy
        p.map_err(|_| DbError::Unknown)
    }

    pub fn del(&self, key: Box<[u8]>) -> impl Future<Item = Option<Arc<[u8]>>, Error = DbError> {
        let t = self.sled_tree.clone();

        let (c, p): (
            oneshot::Sender<Option<Arc<[u8]>>>,
            oneshot::Receiver<Option<Arc<[u8]>>>,
        ) = oneshot::channel();

        let cell = Arc::new(Mutex::new(Some(c)));

        // This is way more complicated than it needs to be.
        // TODO Make this less gross.
        let set_fut = poll_fn({
            move || {
                blocking({
                    let t = t.clone();
                    let k = key.clone();
                    let c = cell
                        .as_ref()
                        .lock()
                        .expect("dbcompletionlock")
                        .take()
                        .expect("dbcompletion");
                    move || {
                        match t.del(k.as_ref()) {
                            Ok(Some(iv)) => {
                                let data: Arc<_> = iv.into();
                                c.send(Some(data));
                                Ok(())
                            }
                            Ok(None) => {
                                c.send(None);
                                Ok(())
                            }
                            Err(e) => {
                                println!("db error: {}", e);
                                Err(())
                            }
                        };
                    }
                })
            }
        });

        util::spawn_task(set_fut.map_err(|_| ()), "dbdel", true); // TODO Make not noisy
        p.map_err(|_| DbError::Unknown)
    }

    // TODO compare-and-swap?
}

#[derive(Copy, Clone, Hash, Debug, Serialize, Deserialize)]
enum UserColor {
    Red,
    Blue,
    Green,
    Yellow,
}

#[derive(Copy, Clone, Hash, Debug, Serialize, Deserialize)]
struct UserProfile {
    color: UserColor,
    spent_sat: u64,
}
